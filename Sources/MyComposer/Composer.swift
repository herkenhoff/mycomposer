//
//  Composer.swift
//  VRManager
//
//  Created by Christian Herkenhoff on 21.02.17.
//  Copyright © 2017 Christian Herkenhoff. All rights reserved.
//

import UIKit

public protocol Composer {
    
    var fileName: String { get }
    var cssStyleFileName: String? { get }
    var pageFrame: CGRect { get }
    func render() -> String
    func getPrintPageRenderer(_ pageFrame: CGRect) -> UIPrintPageRenderer;
}

public extension Composer {
    
    var pdfFileName: String {
        return "\(filePath).pdf"
    }
    
    var content: String {
        get {
            var content = render()
            if let cssStyleFileName = self.cssStyleFileName {
                content.replaceing([
                "cssStyle":getCSSFile(cssStyleFileName)
                ])
            }
            return content
        }
    }
    
    func getPrintPageRenderer(_ pageFrame: CGRect) -> UIPrintPageRenderer {
        return CustomPrintPageRenderer(pageFrame)
    }
    
    func toHTML(){
        let renderedContent = self.content
        self.toHTML(content: renderedContent)
    }
    
    func toPDF() {
        let renderedContent = self.content
        let printFormatter = UIMarkupTextPrintFormatter(markupText: renderedContent)
        self.toPDF(printFormatter: printFormatter)
    }
}

extension Composer {
    
    func toPDF(printFormatter: UIPrintFormatter) {
        let printPageRenderer = getPrintPageRenderer(pageFrame)
        printPageRenderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
        
        let pdfData = drawPDFUsingPrintPageRenderer(printPageRenderer: printPageRenderer)
        pdfData?.write(toFile: pdfFileName, atomically: true)
        
        print(pdfFileName)
    }
    
    private func toHTML(content: String){
        try? content.write(to: URL(fileURLWithPath: htmlFileName), atomically: false, encoding: .utf8)
    }
    
    private func getCSSFile(_ fileName: String) -> String {
        do{
            let path = Bundle.main.path(forResource: fileName, ofType: "css")
            return try String(contentsOfFile: path!, encoding: .utf8)
        }catch{
            return ""
        }
    }
    
    private var htmlFileName: String {
        return "\(filePath).html"
    }
    
    private var filePath: String {
        let rootDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        
        return "\(rootDirectory)/\(fileName)"
    }
    
    private func drawPDFUsingPrintPageRenderer(printPageRenderer: UIPrintPageRenderer) -> NSData! {
        let data = NSMutableData()
        
        UIGraphicsBeginPDFContextToData(data, pageFrame, nil)
        printPageRenderer.prepare(forDrawingPages: NSMakeRange(0, printPageRenderer.numberOfPages))

        for page in 0 ... printPageRenderer.numberOfPages-1 {
            UIGraphicsBeginPDFPage()
            printPageRenderer.drawPage(at: page, in: UIGraphicsGetPDFContextBounds())
        }
        
        UIGraphicsEndPDFContext()
        
        return data
    }
}
