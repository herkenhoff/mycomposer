//
//  ComposerPreviewController.swift
//  VRManager
//
//  Created by Christian Herkenhoff on 14.10.17.
//  Copyright © 2017 Christian Herkenhoff. All rights reserved.
//

import UIKit
import WebKit

public class ComposerPreviewController: UIViewController {

    var documentInteractionController: UIDocumentInteractionController?
    var webView: WKWebView?

    var composer: Composer?
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
    }
    
    public init(composer: Composer){
        super.init(nibName: nil, bundle: nil)
        
        self.composer = composer
        
        self.title = self.composer!.fileName
        
        self.webView = WKWebView()
        self.webView!.translatesAutoresizingMaskIntoConstraints = false
        self.webView!.contentMode = .scaleAspectFit;
        self.webView!.loadHTMLString(self.composer!.content, baseURL: nil)
        
        self.view.addSubview(self.webView!)
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self.webView!]))
        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self.webView!]))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "PDF erstellen", style: .plain, target: self, action: #selector(createPdf))
        
        self.documentInteractionController = UIDocumentInteractionController()
        self.documentInteractionController!.delegate = self
    }

    @objc func createPdf() {
        let printFormatter = self.webView!.viewPrintFormatter()
        printFormatter.perPageContentInsets = UIEdgeInsets.zero
        
        composer?.toPDF(printFormatter: printFormatter)
        
        self.documentInteractionController!.url = URL(fileURLWithPath: self.composer!.pdfFileName)
        self.documentInteractionController!.presentPreview(animated: true)
    }
}

extension ComposerPreviewController: UIDocumentInteractionControllerDelegate {
    
    public func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
}

