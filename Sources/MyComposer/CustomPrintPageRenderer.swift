//
//  CustomPrintRenderer.swift
//  VRManager
//
//  Created by Christian Herkenhoff on 21.02.17.
//  Copyright © 2017 Christian Herkenhoff. All rights reserved.
//

import UIKit

public struct PageOrientation {
    
    public static let A4PageWidth: CGFloat = 595.2
    public static let A4PageHeight: CGFloat = 841.8
    
    public static let Portrait = CGRect(x: 0.0, y: 0.0, width: A4PageWidth, height: A4PageHeight)
    public static let Landscape = CGRect(x: 0.0, y: 0.0, width: A4PageHeight, height: A4PageWidth)
}

public class CustomPrintPageRenderer: UIPrintPageRenderer {
    
    public init(_ pageFrame: CGRect) {
        super.init()
        
        self.setValue(NSValue(cgRect: pageFrame), forKey: "paperRect")
        self.setValue(NSValue(cgRect: pageFrame.insetBy(dx: 0.0, dy: 40.0)), forKey: "printableRect")
    }
    
}
