//
//  File.swift
//  
//
//  Created by Christian Herkenhoff on 01.05.20.
//

import Foundation

public extension String {
    
    var localizedFileName: String {
        let currentLanguage =  NSLocale.current.languageCode ?? "en"
        return String.init(format: self, currentLanguage)
    }
    
    func getHtmlTemplate() -> String {
        do{
            let path = Bundle.main.path(forResource: self, ofType: "html")
            return try String(contentsOfFile: path!, encoding: .utf8)
        }catch (let ex){
            print(ex)
            return ""
        }
    }
    
    func getLocalizedTemplate() -> String {
        var fileName = "\(self)_%@".localizedFileName
        
        if !FileManager.default.fileExists(atPath: fileName) {
            fileName = "\(self)_de"
        }
        return fileName.getHtmlTemplate()
    }
    
}

extension String {
    
    func replace(_ keyValuePairs: Dictionary<String, String>) -> String {
        return self.replace(keyValuePairs, tag: "#")
    }
    
    mutating func replaceing(_ keyValuePairs: Dictionary<String, String>){
        self = self.replace(keyValuePairs)
    }
    
    func replace(_ keyValuePairs: Dictionary<String, String>, tag: String) -> String {
        var template = self
        for kvp in keyValuePairs {
            template = template.replacingOccurrences(of: "\(tag)\(kvp.key)\(tag)", with: kvp.value)
        }
        return template
    }
}

extension NSDecimalNumber {
    
    func formattedString(_ numberOfFactionDigits: Int) -> String {
        let formatter = NumberFormatter.formatter(minimumFractionDigits: numberOfFactionDigits)
        return formatter.string(from: self)!
    }
    
}

extension NumberFormatter {
    
    class func defaultFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        
        formatter.locale = Locale.current
        formatter.usesGroupingSeparator = true
        formatter.maximumFractionDigits = 2
        
        return formatter
    }
    
    class func formatter(minimumFractionDigits: Int) -> NumberFormatter {
        let formatter = NumberFormatter.defaultFormatter()
        formatter.minimumFractionDigits = minimumFractionDigits
        formatter.numberStyle = minimumFractionDigits == 0 ? .none : .decimal
        
        return formatter
    }
    
}
