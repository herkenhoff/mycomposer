//
//  ListComposer.swift
//  VRManager
//
//  Created by Christian Herkenhoff on 30.06.17.
//  Copyright © 2017 Christian Herkenhoff. All rights reserved.
//

import Foundation
import MyHtmlBuilder

public protocol ListComposer : Composer {
    
    var title: String { get }
    var subTitle: String { get }
    var columnNames: [HtmlTableHeadCell] { get }
    var rows: [HtmlTableRow] { get }
    
}

public extension ListComposer {
        
    func getPageBreak() -> HtmlTableRow {
        let pageBreak = HtmlTableRow()
        
        pageBreak.cells.append(HtmlTableCell(value: HtmlBuilder.make(tag: HtmlTag.div, value: "", class: "pagebreak")))
        
        return pageBreak
    }
    
    func getTableHeader() -> HtmlTableRow{
        let header = HtmlTableRow()
        
        for column in columnNames{
            header.cells.append(column)
        }
        return header
    }
    
    func getHeader(_ title: String) -> HtmlTableRow {
        let row = HtmlTableRow()

        row.attributes = [HtmlAttribute(key: "class", value: "header")]
        let cell = HtmlTableCell(value: HtmlBuilder.make(tag: HtmlTag.b, value: title)).addingColspan(self.columnNames.count)
        
        row.cells.append(cell)
        
        return row
    }
    
    func getFooter(_ text: String, value: NSDecimalNumber) -> HtmlTableRow {
        let footer = HtmlTableRow()
        
        footer.attributes = [HtmlAttribute(key: "class", value: "sum")]
        footer.cells.append(HtmlTableCell(value: text).addingColspan(self.columnNames.count-1))
        footer.cells.append(HtmlTableCell(value: value.formattedString(2), className:"number"))
        
        return footer
    }
    
    func getFooter(_ text: String, values: [NSDecimalNumber]) -> HtmlTableRow {
        let row = HtmlTableRow()
        
        row.attributes = [HtmlAttribute(key: "class", value: "sum")]
        row.cells.append(HtmlTableCell(value: text).addingColspan(self.columnNames.count - values.count))
        row.cells.append(contentsOf: values.map({ HtmlTableCell(value: $0.formattedString(2), className:"number") }))
        
        return row
    }
    
    func render() -> String {
        let htmlTable = HtmlTable()
        
        htmlTable.rows.append(self.getTableHeader())
        htmlTable.rows.append(contentsOf: self.rows)
        
        let divTag = HtmlContainerElement(tag: HtmlTag.div)
        divTag.attributes = [HtmlAttribute(key: "class", value: "content")]
        divTag.elements = [
            HtmlElementBase(tag: HtmlTag.h1, value: self.title),
            HtmlElementBase(tag: HtmlTag.h2, value: self.subTitle),
            htmlTable
        ]

        let html = Html()
        html.head.title = self.title
        html.head.style = "#cssStyle#"
        html.body.append(divTag)
                
        return html.render()
    }
    
}
