import XCTest

import MyComposerTests

var tests = [XCTestCaseEntry]()
tests += MyComposerTests.allTests()
XCTMain(tests)
